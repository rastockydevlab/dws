default: build run clean

build:
	@echo "Building..."
	docker compose build --quiet

run:
	@echo "Running..."
	docker compose up

clean:
	@echo "Cleaning..."
	docker compose down --rmi all --volumes --remove-orphans