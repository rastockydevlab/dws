import { RRule, datetime } from 'rrule'
import logger from '../config/logger.config'

import { Router } from 'express'

const router = Router()

import { AppDataSource as db } from "../config/db"
import { RecurringEvent } from '../entity/RecurringEvent'
import { Event } from '../entity/Event'

router.get('/', async (req, res) => {
    const recurringEvent = await db.manager.find(RecurringEvent)
    res.json({
        recurringEvent
    })
})

router.post('/', async (req, res) => {
    const recurringEvent = new RecurringEvent()
    recurringEvent.title = req.body.title
    recurringEvent.start = req.body.start
    recurringEvent.end = req.body.end
    recurringEvent.rrule = req.body.rrule

    await db.manager.save(recurringEvent)

    res.status(201).json({
        recurringEvent
    })
})

router.get('/:id', async (req, res) => {
    const recurringEvent = await db.manager.findOne(RecurringEvent, {
        where: {
            id: req.params.id
        }
    })

    if (!recurringEvent) {
        res.status(404).json({
            message: 'RecurringEvent not found'
        })
        return
    }

    res.json({
        recurringEvent
    })
})

router.put('/:id', async (req, res) => {
    const recurringEvent = await db.manager.findOne(RecurringEvent, {
        where: {
            id: req.params.id
        }
    })

    if (!recurringEvent) {
        res.status(404).json({
            message: 'RecurringEvent not found'
        })
        return
    }

    recurringEvent.title = req.body.title
    recurringEvent.start = req.body.start
    recurringEvent.end = req.body.end
    recurringEvent.rrule = req.body.rrule

    await db.manager.save(recurringEvent)

    res.json({
        recurringEvent
    })
})

router.delete('/:id', async (req, res) => {
    const recurringEvent = await db.manager.findOne(RecurringEvent, {
        where: {
            id: req.params.id
        }
    })

    if (!recurringEvent) {
        res.status(404).json({
            message: 'RecurringEvent not found'
        })
        return
    }

    await db.manager.remove(recurringEvent)

    res.json({
        message: 'RecurringEvent deleted'
    })
})

// Generate ocurrences
router.post('/generate-occurences', async (req, res) => {
    const { start, end } = req.body
    
    if (!start || !end) {
        res.status(400).json({
            message: 'start and end are required'
        })

        return
    }

    const startDate = new Date(start)
    const endDate = new Date(end)

    const recurringEvents = await db.manager.find(RecurringEvent)

    recurringEvents.forEach(recurringEvent => {
        const rule = RRule.fromString(recurringEvent.rrule)
        
        const dates = rule.between(
            datetime(startDate.getUTCFullYear(), startDate.getUTCMonth() + 1, startDate.getUTCDate()),
            datetime(endDate.getUTCFullYear(), endDate.getUTCMonth() + 1, endDate.getUTCDate())
        )
        
        dates.forEach(async date => {
            const event = new Event()

            event.title = recurringEvent.title
            event.start = new Date(date)
            event.start.setHours(recurringEvent.start.getHours())
            event.start.setMinutes(recurringEvent.start.getMinutes())
            event.start.setSeconds(recurringEvent.start.getSeconds())

            event.end = new Date(date)
            event.end.setHours(recurringEvent.end.getHours())
            event.end.setMinutes(recurringEvent.end.getMinutes())
            event.end.setSeconds(recurringEvent.end.getSeconds())

            event.recurringEvent = recurringEvent
    
            await db.manager.save(event)
        })
    })

    res.json({message: 'ok'})
})

export default router