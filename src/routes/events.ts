import { Router } from 'express'

const router = Router()

import { AppDataSource as db } from "../config/db"
import { Event } from "../entity/Event"

router.get('/', async (req, res) => {
    const events = await db.manager.find(Event, {
        relations: {
            recurringEvent: true
        },
        order: {
            start: 'ASC'
        }
    })

    res.json({
        events
    })
})

router.post('/', async (req, res) => {
    const event = new Event()
    event.title = req.body.title
    event.start = req.body.start
    event.end = req.body.end

    await db.manager.save(event)

    res.status(201).json({
        event
    })
})

router.get('/:id', async (req, res) => {
    const event = await db.manager.findOne(Event, {
        where: {
            id: req.params.id
        }
    })

    if (!event) {
        res.status(404).json({
            message: 'Event not found'
        })
        return
    }

    res.json({
        event
    })
})

router.put('/:id', async (req, res) => {
    const event = await db.manager.findOne(Event, {
        where: {
            id: req.params.id
        }
    })

    if (!event) {
        res.status(404).json({
            message: 'Event not found'
        })
        return
    }

    event.title = req.body.title
    event.start = req.body.start
    event.end = req.body.end

    await db.manager.save(event)

    res.json({
        event
    })
})

router.delete('/:id', async (req, res) => {
    const event = await db.manager.findOne(Event, {
        where: {
            id: req.params.id
        }
    })

    if (!event) {
        res.status(404).json({
            message: 'Event not found'
        })
        return
    }

    await db.manager.remove(event)

    res.json({
        message: 'Event deleted'
    })
})

export default router