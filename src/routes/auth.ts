import passport from 'passport'
import bcrypt, { hash } from 'bcrypt'
import jwt from 'jsonwebtoken'

import logger from '../config/logger.config'

import { Router } from 'express'

const router = Router()

// DB and Models
import { AppDataSource as db } from "../config/db"
import { User } from "../entity/User"

// Middleware
import authorized from '../middleware/authorized'

// Auth
router.post('/register', async (req, res) => {
    try {
        // Parse body
        const { email, password, name } = req.body

        // Validate body
        const userWithSameEmail = await db.manager.findOne(User, {
            where: {
                email
            }
        })

        if (userWithSameEmail) throw new Error('Email already exists')

        // Hash password
        const hashedPassword = await bcrypt.hash(password, 10)

        // Create user
        const user = new User()
        user.name = name
        user.email = email
        user.password = hashedPassword

        // Save user
        const newUser = await db.manager.save(user)

        // Generate token
        const token = jwt.sign({ id: newUser.id }, process.env.JWT_SECRET || ''
        )

        // Send token
        res.json(token)
    } catch (err) {
        logger.error(err)
        res.status(500).json({ message: 'Error registering user'})
    }
})

router.post('/login', async (req, res) => {
    try {
        // Parse body
        const { email, password } = req.body

        // Check if user exists
        const user = await db.manager.findOneOrFail(User, {
            where: {
                email
            }
        })

        if (!user) throw new Error('Invalid credentials')

        // Check password
        const isMatch = await bcrypt.compare(password, user.password)

        if (!isMatch) throw new Error('Invalid credentials')

        // Generate token
        const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET || '')

        res.json(token)
    } catch (err) {
        logger.error('Login: ' + err)
        res.status(500).json({ message: 'Error logging in user'})
    }
})

router.get('/protected', authorized, (req, res) => {
    res.json({
        message: 'Protected route',
        user: req.user
    })
})

export default router