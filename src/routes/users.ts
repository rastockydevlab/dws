import { Router } from 'express'

const router = Router()

import { AppDataSource as db } from '../config/db'
import { User } from '../entity/User'
import { Event } from '../entity/Event'
import logger from '../config/logger.config'

router.get('/', async (req, res) => {
    const userRepo = db.getRepository(User)
    
    // find all users and their events ordered by start date
    const users = await userRepo
        .createQueryBuilder('user')
        .leftJoinAndSelect('user.events', 'event')
        .orderBy('event.start', 'ASC')
        .getMany()

    res.json(users)
})

router.post('/', async (req, res) => {
    // validate all fields
    if (Object.keys(req.body).length !== 3) {
        res.status(400).json({
            message: 'All fields are required'
        })
        return
    }

    // check if email is unique
    const userWithSameEmail = await db.manager.findOne(User, {
        where: {
            email: req.body.email
        }
    })

    if (userWithSameEmail) {
        res.status(400).json({
            message: 'Email already exists'
        })
        return
    }

    const user = new User()
    user.name = req.body.name
    user.email = req.body.email
    user.password = req.body.password

    await db.manager.save(user)

    res.status(201).json(user)
})

router.get('/:id', async (req, res) => {
    const user = await db.manager.findOne(User, {
        where: {
            id: req.params.id
        },
        relations: ['events']
    })

    res.json(user)
})

router.put('/:id', async (req, res) => {
    // get user
    const user = await db.manager.findOne(User, {
        where: {
            id: req.params.id
        }
    })

    // check if user exists
    if (!user) {
        res.status(404).json({
            message: 'User not found'
        })
        return
    }

    // check if email is unique
    const userWithSameEmail = await db.manager.findOne(User, {
        where: {
            email: req.body.email
        }
    })

    if (userWithSameEmail && userWithSameEmail.id !== user.id) {
        res.status(400).json({
            message: 'Email already exists'
        })
        return
    }

    // update user
    user.name = req.body.name
    user.email = req.body.email
    user.password = req.body.password

    await db.manager.save(user)

    res.json(user)
})

router.delete('/:id', async (req, res) => {
    // get user
    const user = await db.manager.findOne(User, {
        where: {
            id: req.params.id
        }
    })

    // check if user exists
    if (!user) {
        res.status(404).json({
            message: 'User not found'
        })
        return
    }

    // delete user
    await db.manager.delete(User, user.id)

    res.json({
        message: 'User deleted'
    })
})

// add event to user
router.post('/:id/events', async (req, res) => {
    try {
        const user = await db.manager.findOneOrFail(User, {
            where: {
                id: req.params.id
            },
            relations: ['events']
        })

        console.log(user)

        const event = await db.manager.findOneOrFail(Event, {
            where: {
                id: req.body.eventId
            }
        })

        console.log(event)

        user?.events.push(event)

        await db.manager.save(user)

        res.status(201).json(user)
    } catch (err) {
        logger.error(err)

        res.status(500).json({
            message: err
        })
    }
})

// remove event from user
router.delete('/:id/events/:eventId', async (req, res) => {
    try {
        const user = await db.manager.findOneOrFail(User, {
            where: {
                id: req.params.id
            },
            relations: ['events']
        })

        const event = await db.manager.findOneOrFail(Event, {
            where: {
                id: req.params.eventId
            }
        })

        user?.events.splice(user.events.indexOf(event), 1)

        await db.manager.save(user)

        res.json({
            message: 'Event removed from user'
        })
    } catch (err) {
        logger.error(err)

        res.status(500).json({
            message: err
        })
    }
})

export default router