// Setup
import dotenv from 'dotenv'
dotenv.config()

import 'reflect-metadata'

import logger from './config/logger.config'


// Express
import express from 'express'
const app = express()
const port = process.env.PORT || 3000
app.use(express.json())

import passport from 'passport'
import './config/passport'
app.use(passport.initialize())


// Database
import { AppDataSource as db } from "./config/db"

import seededUsers from './seeds/user.seeds'
import seededEvents from './seeds/event.seeds'
import seededRecurringEvents from './seeds/recurringEvent.seeds'


// Routes
import authRouter from './routes/auth'
import eventsRouter from './routes/events'
import usersRouter from './routes/users'
import recurringEventsRouter from './routes/recurringEvents'

async function main() {
    try {
        await db.initialize()    
        logger.debug('Database initialized')
        
        await db.manager.save(seededEvents)
        await db.manager.save(seededRecurringEvents)
        await db.manager.save(seededUsers)
        logger.debug('Database seeded')
    } catch (error) {
        logger.error('Error initializing database: ', error)
    }
    

    // Routes
    app.get('/', (req, res) => {
        res.json({
            status: 'OK'
        })        
    })

    app.use('/auth', authRouter)
    app.use('/events', eventsRouter)
    app.use('/users', usersRouter)
    app.use('/recurring-events', recurringEventsRouter)
    

    // Start the app
    app.listen(port, () => {
        logger.info(`Server running at http://localhost:${port}`)
    })
}

// Let's roll 😎
main()