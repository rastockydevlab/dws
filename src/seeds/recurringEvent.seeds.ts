import { RecurringEvent } from "../entity/RecurringEvent"

import seededEvents from "./event.seeds"

const obedy = seededEvents.slice(2, 12)
obedy.splice(6, 1)

const vecere = seededEvents.slice(12, 22)

const obed = new RecurringEvent()
obed.id = '33333333-0bfe-4c6b-9991-171d73a00389'
obed.title = 'Obed'
obed.start = new Date('2023-05-01T13:00:00')
obed.end = new Date('2023-05-01T14:00:00')
obed.rrule = 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA'
obed.events = obedy

const obedNedela = new RecurringEvent()
obedNedela.id = '33333333-0bfe-4c6b-9991-171d73a00390'
obedNedela.title = 'Obed'
obedNedela.start = new Date('2023-05-01T14:00:00')
obedNedela.end = new Date('2023-05-01T15:00:00')
obedNedela.rrule = 'FREQ=WEEKLY;BYDAY=SU'
obedNedela.events = [seededEvents[8]]

const vecera = new RecurringEvent()
vecera.id = '33333333-0bfe-4c6b-9991-171d73a00391'
vecera.title = 'Večera'
vecera.start = new Date('2023-05-08T20:15:00')
vecera.end = new Date('2023-05-08T22:00:00')
vecera.rrule = 'FREQ=DAILY'
vecera.events = vecere


const seededRecurringEvents = [obed, obedNedela, vecera]

export default seededRecurringEvents
