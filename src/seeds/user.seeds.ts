import bcrypt from 'bcrypt'

import { User } from '../entity/User'

import seededEvents from './event.seeds'

const admin = new User()
admin.id = '11111111-0bfe-4c6b-9991-171d73a00388'
admin.name = 'Super Admin'
admin.email = 'superadmin@admin.com'
admin.password = '123456'

const matej = new User()
matej.id = '11111111-0bfe-4c6b-9991-171d73a00389'
matej.name = 'Matej Rástocký'
matej.email = 'rastockymatej@gmail.com'
matej.password = 'password'
matej.events = [
    seededEvents[0],
    seededEvents[1],
    seededEvents[7],
    seededEvents[9],
    seededEvents[10],
    seededEvents[17],
    seededEvents[18],
    seededEvents[19],
    seededEvents[20],
    seededEvents[21],
]

const vajro = new User()
vajro.id = '11111111-0bfe-4c6b-9991-171d73a00390'
vajro.name = 'Ondrej Kováč'
vajro.email = 'ondrej.vajansky@gmail.com'
vajro.password = 'password'
vajro.events = [
    seededEvents[0],
    seededEvents[5],
    seededEvents[6],
    seededEvents[7],
    seededEvents[8],
    seededEvents[9],
    seededEvents[10],
    seededEvents[11],
    seededEvents[12],
    seededEvents[13],
    seededEvents[14],
    seededEvents[15],
    seededEvents[16],
    seededEvents[17],
    seededEvents[18],
    seededEvents[19],
    seededEvents[20],
    seededEvents[21],
]

const petr = new User()
petr.id = '11111111-0bfe-4c6b-9991-171d73a00391'
petr.name = 'Petř Dušek'
petr.email = 'petr.dusek@gmail.com',
petr.password = 'password'
petr.events = [
    seededEvents[1],
    seededEvents[5],
    seededEvents[6],
    seededEvents[7],
    seededEvents[8],
    seededEvents[9],
    seededEvents[10],
    seededEvents[11],
    seededEvents[12],
    seededEvents[13],
    seededEvents[14],
    seededEvents[15],
    seededEvents[16],
    seededEvents[17],
    seededEvents[18],
    seededEvents[19],
    seededEvents[20],
    seededEvents[21],
]


const seededUsers = [
    admin,
    matej,
    vajro,
    petr
]

const hashedUsers = seededUsers.map(async user => {
    user.password = await bcrypt.hash(user.password, 10)
})

export default seededUsers
