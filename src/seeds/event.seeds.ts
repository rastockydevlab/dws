import { Event } from '../entity/Event'

const poker = new Event()
poker.id = '22222222-0bfe-4c6b-9991-171d73a00389'
poker.title = 'Pokrový turnaj'
poker.start = new Date('2023-05-06T18:00:00')
poker.end = new Date('2023-05-06T23:59:59')

const johnwick = new Event()
johnwick.id = '22222222-0bfe-4c6b-9991-171d73a00390'
johnwick.title = 'Kino: John Wick 4'
johnwick.start = new Date('2023-05-08T19:40:00')
johnwick.end = new Date('2023-05-08T22:00:00')

function EventFactory (title: string, start: Date, end: Date) {
    const event = new Event()
    event.title = title
    event.start = start
    event.end = end
    return event
}

const obedy = [
    EventFactory('Obed', new Date('2023-05-01T13:00:00'), new Date('2023-05-01T14:00:00')),
    EventFactory('Obed', new Date('2023-05-02T13:00:00'), new Date('2023-05-02T14:00:00')),
    EventFactory('Obed', new Date('2023-05-03T13:00:00'), new Date('2023-05-03T14:00:00')),
    EventFactory('Obed', new Date('2023-05-04T13:00:00'), new Date('2023-05-04T14:00:00')),
    EventFactory('Obed', new Date('2023-05-05T13:00:00'), new Date('2023-05-05T14:00:00')),
    EventFactory('Obed', new Date('2023-05-06T13:00:00'), new Date('2023-05-06T14:00:00')),
    EventFactory('Obed', new Date('2023-05-07T14:00:00'), new Date('2023-05-07T15:00:00')),
    EventFactory('Obed', new Date('2023-05-08T13:00:00'), new Date('2023-05-08T14:00:00')),
    EventFactory('Obed', new Date('2023-05-09T13:00:00'), new Date('2023-05-09T14:00:00')),
    EventFactory('Obed', new Date('2023-05-10T13:00:00'), new Date('2023-05-10T14:00:00')),
]

const vecere = [
    EventFactory('Večera', new Date('2023-05-01T20:15:00'), new Date('2023-05-01T22:00:00')),
    EventFactory('Večera', new Date('2023-05-02T20:15:00'), new Date('2023-05-02T22:00:00')),
    EventFactory('Večera', new Date('2023-05-03T20:15:00'), new Date('2023-05-03T22:00:00')),
    EventFactory('Večera', new Date('2023-05-04T20:15:00'), new Date('2023-05-04T22:00:00')),
    EventFactory('Večera', new Date('2023-05-05T20:15:00'), new Date('2023-05-05T22:00:00')),
    EventFactory('Večera', new Date('2023-05-06T20:15:00'), new Date('2023-05-06T22:00:00')),
    EventFactory('Večera', new Date('2023-05-07T20:15:00'), new Date('2023-05-07T22:00:00')),
    EventFactory('Večera', new Date('2023-05-08T20:15:00'), new Date('2023-05-08T22:00:00')),
    EventFactory('Večera', new Date('2023-05-09T20:15:00'), new Date('2023-05-09T22:00:00')),
    EventFactory('Večera', new Date('2023-05-10T20:15:00'), new Date('2023-05-10T22:00:00')),
]


const seededEvents = [poker, johnwick, ...obedy, ...vecere]

export default seededEvents
