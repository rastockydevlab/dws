import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, OneToMany } from "typeorm"

import { Event } from "./Event"

@Entity('recurringevents')
export class RecurringEvent extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    title: string

    @Column()
    start: Date

    @Column()
    end: Date

    @Column()
    rrule: string

    @OneToMany(() => Event, event => event.recurringEvent, {
        onDelete: 'CASCADE'
    })
    events: Event[]

    @CreateDateColumn()
    created_at: Date

    @UpdateDateColumn()
    updated_at: Date
}
