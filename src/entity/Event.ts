import {
    Entity,
    BaseEntity,
    PrimaryGeneratedColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
    ManyToOne,
    ManyToMany,
    JoinTable
} from "typeorm"

import { RecurringEvent } from "./RecurringEvent"
import { User } from "./User"

@Entity('events')
export class Event extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    title: string

    @Column()
    start: Date

    @Column()
    end: Date

    @ManyToOne(() => RecurringEvent, recurringEvent => recurringEvent.events, {
        onDelete: 'CASCADE'
    })
    recurringEvent: RecurringEvent

    @ManyToMany(() => User, user => user.events, {
        onDelete: 'CASCADE'
    })
    @JoinTable()
    users: User[]

    @CreateDateColumn()
    created_at: Date

    @UpdateDateColumn()
    updated_at: Date
}
