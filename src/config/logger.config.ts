import winston from 'winston'

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      level: 'silly',
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.timestamp({format: 'DD-MM-YYYY HH:mm:ss'}),
        winston.format.printf(info => `${info.level} | ${info.message}`)
      )
    })
  ]
})

export default logger