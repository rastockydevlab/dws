import "reflect-metadata"
import { DataSource } from "typeorm"

import { User } from "../entity/User"
import { RecurringEvent } from "../entity/RecurringEvent"
import { Event } from "../entity/Event"

export const AppDataSource = new DataSource({
    type: "postgres",
    host: process.env.DB_HOST,
    port: 5432,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    synchronize: true,
    dropSchema: true,
    logging: false,
    logger: "advanced-console",
    entities: [User, RecurringEvent, Event],
    migrations: [],
    subscribers: [],
})
