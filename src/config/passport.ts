import passport from 'passport'

import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt'

import { AppDataSource as db } from '../config/db'
import { User } from '../entity/User'
import logger from './logger.config'

const jwtSecret = process.env.JWT_SECRET

if (!jwtSecret) {
    throw new Error('JWT_SECRET not found in environment')
}

const jwtOpts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret
}

passport.use(
    new JwtStrategy(jwtOpts, async (jwtPayload, done) => {
        try {
            const user = await db.manager.findOneOrFail(User, {
                where: {
                    id: jwtPayload.id
                }
            })
            
            if (user)
                return done(null, user)
            
            return done(null, false, { message: 'Invalid token' })
        } catch (error) {
            logger.error('Passport: ' + error)
            return done(error, false)
        }
    })
)